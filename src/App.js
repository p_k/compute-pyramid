import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(){
    super();
    this.state = {
      result : undefined,
      value : 0
    }
  }

  Compute = () => {
    let response = {
      status : 'false',
      floor : 0 
    }
      for(let i = 1; i <= this.state.value; i+= (response.floor+1)) {
        if(i == this.state.value) {
          response.status = 'true';
        }
        response.floor++;
      }
      this.setState({
        result : response
      })
  }


  onChangeInput = (e , v) => {
    if(isNaN(Number(e.target.value))) {
      e.target.value = '';
    } else {
      e.target.value = Number(e.target.value);
    }
    this.setState({
      value : e.target.value
    })
  }

  render() {
    return (
      <div className="App">
        <div className="card">
          <div className="input-card">
            <label>input brick :</label>
            <input value={this.state.value} onChange={this.onChangeInput} type="number" />
            <button onClick={this.Compute}>compute</button>
          </div>

        </div>
        <div>
          {this.state.result ? 
            `result : ${this.state.result.floor} floor and success ${ this.state.result.status } `
            : `result : not result `}
            
          </div>
      </div>
    );
  }
}

export default App;
